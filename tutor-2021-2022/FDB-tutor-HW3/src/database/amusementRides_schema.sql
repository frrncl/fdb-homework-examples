-- Database Creation
CREATE DATABASE tutordb OWNER parkadmin ENCODING = 'UTF8';

-- Connect to the new db
\c tutordb

-- Create new Schema (you can use the public schema without crating a new one, if you want)
DROP SCHEMA IF EXISTS AmusementRides CASCADE;
CREATE SCHEMA  AmusementRides;

-- Create new domains
CREATE DOMAIN AmusementRides.passwd AS VARCHAR(254)
	CONSTRAINT properpassword CHECK (((VALUE)::text ~* '[A-Za-z0-9._%!]{8,}'::text));

CREATE DOMAIN AmusementRides.emailaddress AS VARCHAR(254)
	CONSTRAINT properemail CHECK (((VALUE)::text ~* '^[A-Za-z0-9._%]+@[A-Za-z0-9.]+[.][A-Za-z]+$'::text));

CREATE DOMAIN AmusementRides.vatnumber AS VARCHAR(15)
	CONSTRAINT properVAT CHECK (((VALUE)::text ~* '[A-Za-z0-9]{5,}'::text));

CREATE DOMAIN AmusementRides.rideserial AS CHAR(10)
	CONSTRAINT properserial CHECK (((VALUE)::text ~* '[A-Z]{3}[0-9]{7}'::text));

CREATE DOMAIN AmusementRides.deviceserial AS CHAR(10)
	CONSTRAINT properserial CHECK (((VALUE)::text ~* '[0-9]{10}'::text));
	
-- Create new data types
CREATE TYPE AmusementRides.roles AS ENUM (
    'Administrator',
    'CommercialEmployee',
    'MaintenanceTechnician',
	'DataAnalyst',
	'ParkTechnician',
	'NotAssigned'
);

CREATE TYPE AmusementRides.eventtype AS ENUM (
    'OrdinaryMaintenance',
    'ExtraordinaryMaintenance',
    'HardwareUpgrade'
);

CREATE TYPE AmusementRides.sessiontype AS ENUM (
    'Maintenance',
    'Test',
    'Monitoring'
);
CREATE TYPE AmusementRides.devicetype AS ENUM (
    'PLC',
    'WeatherStation',
    'other'
);

-- Table Creation (create tables in the correct order, i.e. use FKeys only referring to already created tables)


CREATE TABLE AmusementRides.park (
    name VARCHAR(32) PRIMARY KEY,
    email AmusementRides.EMAILADDRESS NOT NULL
);

CREATE TABLE AmusementRides.company (
    vat AmusementRides.VATNUMBER PRIMARY KEY,
    name VARCHAR(32) NOT NULL,
    email AmusementRides.EMAILADDRESS
);

CREATE TABLE AmusementRides.user (
    email AmusementRides.EMAILADDRESS PRIMARY KEY,
	password AmusementRides.PASSWD,
    first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	role AmusementRides.ROLES DEFAULT 'NotAssigned',
	parkid  VARCHAR(32),
	companyid AmusementRides.VATNUMBER,
	FOREIGN KEY(parkid) REFERENCES AmusementRides.park(name),
	FOREIGN KEY(companyid) REFERENCES AmusementRides.company(vat)
);

CREATE TABLE AmusementRides.model (
    name VARCHAR(50) PRIMARY KEY,
    description TEXT
);

CREATE TABLE AmusementRides.ride (
    id AmusementRides.RIDESERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    description TEXT,
    parkid  VARCHAR(32) NOT NULL,
    modelid VARCHAR(50) NOT NULL,
	FOREIGN KEY (parkid) REFERENCES AmusementRides.park(name),
	FOREIGN KEY (modelid) REFERENCES AmusementRides.model(name)
);

CREATE TABLE AmusementRides.event (
    eventid SERIAL PRIMARY KEY,
    dateperformed TIMESTAMP WITH TIME ZONE NOT NULL,
    rideid AmusementRides.RIDESERIAL NOT NULL,
	userid AmusementRides.EMAILADDRESS NOT NULL,
    description TEXT NOT NULL,
    type AmusementRides.EVENTTYPE NOT NULL,
	FOREIGN KEY(userid) REFERENCES AmusementRides.user(email),
	FOREIGN KEY(rideid) REFERENCES AmusementRides.ride(id)
);


CREATE TABLE AmusementRides.session (
	id SERIAL PRIMARY KEY, 
	filename VARCHAR(100) NOT NULL,
    starttime TIMESTAMP WITH TIME ZONE NOT NULL,
    type AmusementRides.SESSIONTYPE NOT NULL,
	numCycles SMALLINT DEFAULT 0,
	dateadded TIMESTAMP WITH TIME ZONE NOT NULL,
    rideid AmusementRides.RIDESERIAL NOT NULL,
	addedBy AmusementRides.EMAILADDRESS NOT NULL,
	FOREIGN KEY (rideid)  references AmusementRides.ride(id),
	FOREIGN KEY (addedBy) references AmusementRides.user(email)
);

CREATE TABLE AmusementRides.metatag (
    name VARCHAR(32) PRIMARY KEY,
    description TEXT
);

CREATE TABLE AmusementRides.metavalue (
    sessionid INTEGER,
    metatagid VARCHAR(32),
    value VARCHAR(200),
	FOREIGN KEY (sessionid) REFERENCES AmusementRides.session(id),
	FOREIGN KEY (metatagid) REFERENCES AmusementRides.metatag(name),
	PRIMARY KEY (sessionid, metatagid)
);

CREATE TABLE AmusementRides.tag (
    name VARCHAR(64) PRIMARY KEY,
    description TEXT
);

CREATE TABLE AmusementRides.synonym(
    tagid VARCHAR(64),
    synonymid VARCHAR(64),
	FOREIGN KEY (tagid) REFERENCES AmusementRides.tag(name),
	FOREIGN KEY (synonymid) REFERENCES AmusementRides.tag(name),
	PRIMARY KEY (tagid,synonymid)
);

CREATE TABLE AmusementRides.device (
    id AmusementRides.DEVICESERIAL primary key,
    name VARCHAR(32),
    type AmusementRides.DEVICETYPE NOT NULL,
    description TEXT,
	rideid AmusementRides.RIDESERIAL NOT NULL,
	FOREIGN KEY (rideid)  REFERENCES AmusementRides.ride(id)
);

CREATE TABLE AmusementRides.packet (
    id SERIAL PRIMARY KEY,
    dataarray DOUBLE PRECISION[],
    steparray SMALLINT[],
    starttime TIMESTAMP WITH TIME ZONE,
    cycle SMALLINT,
    sessionid INTEGER NOT NULL,
    tagid VARCHAR(64) NOT NULL,
    deviceid Amusementrides.DEVICESERIAL NOT NULL,
	FOREIGN KEY (tagid) REFERENCES AmusementRides.tag(name),
	FOREIGN KEY (deviceid) REFERENCES AmusementRides.device(id),
	FOREIGN KEY (sessionid) REFERENCES AmusementRides.session(id)
);



CREATE TABLE AmusementRides.feature (
    name VARCHAR(32) PRIMARY KEY,
    description TEXT
);

CREATE TABLE AmusementRides.featureValue (
    featureid VARCHAR(32),
    packetid INTEGER,
	value DOUBLE PRECISION NOT NULL,
	FOREIGN KEY (featureid) REFERENCES AmusementRides.feature(name),
	FOREIGN KEY (packetid) REFERENCES AmusementRides.packet(id),
	PRIMARY KEY (featureid,packetid)
);

CREATE TABLE AmusementRides.featureReference (
    featureid VARCHAR(32),
    rideid AmusementRides.RIDESERIAL,
	tagid VARCHAR(64),
	value DOUBLE PRECISION NOT NULL,
	FOREIGN KEY (featureid) REFERENCES AmusementRides.feature(name),
	FOREIGN KEY (rideid) REFERENCES AmusementRides.ride(id),
	FOREIGN KEY (tagid) REFERENCES AmusementRides.tag(name),
	PRIMARY KEY (featureid,rideid,tagid)
);





