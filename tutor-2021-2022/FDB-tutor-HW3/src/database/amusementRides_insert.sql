-- Insert operations

-- Park
INSERT INTO Amusementrides.park(name, email) VALUES
	('AcroparkNY','info@acroparkny.com'),
	('AquaFun','public@aquafun.com'),
	('RideAdventures','customerservice@rideadventures.com');

-- Maintenance Companies
INSERT INTO Amusementrides.company(vat,name, email) VALUES
('0764352056C','PolyTech','service@polytech.com'),
('1243894986B','ParkService','info@parkservice.com');

-- Users
INSERT INTO Amusementrides.user (email, password, first_name, last_name, role, companyid, parkid) VALUES
('John@acroparkny.com', 'P455id32!' ,'John', 'Smith', 'ParkTechnician', NULL, 'AcroparkNY'),
('Paul@acroparkny.com', 'P4tre6poY' ,'Paul', 'Black', 'ParkTechnician', NULL, 'AcroparkNY'),
('service@polytech.com', 'Treef5389' ,'Jenson', 'Lansbury', 'MaintenanceTechnician', '0764352056C', NULL),
('manager@polytech.com', 'ASb!feoRF' ,'Max', 'Todd', 'MaintenanceTechnician', '0764352056C', NULL),
('info@parkservice.com', 'Frhu5djow' ,'Peter', 'Karls', 'MaintenanceTechnician', '1243894986B', NULL);
	
-- Models

INSERT INTO Amusementrides.Model VALUES
('TowerLift', 'TowerLift Model, 2010, 8 seats, Max load 2000 Kg, link: www.amusementRides.com/rides/Towerlift'),
('Coaster', 'Coaster Model, 2002, 10 double seats (max 20 people), Max load 5000 Kg, link: www.amusementRides.com/rides/Coaster');
	
-- Rides

INSERT INTO Amusementrides.Ride VALUES
('CST0000001','High Mountains', 'Installed in 2010, Position: pitch 15','AcroparkNY','Coaster'),
('CST0000002','Kids Mountains', 'Installed in 2010, Position: pitch 14','AcroparkNY','Coaster'),
('TLF0000001','ThrillerFall', 'Installed in 2015, Position: pitch 7','AcroparkNY','TowerLift');

-- Maintenance Events
INSERT INTO Amusementrides.event(type,description,Rideid,userid,dateperformed) VALUES
('OrdinaryMaintenance','Complete Chain Lubrication','CST0000001','service@polytech.com','2010-05-03 17:30:00'),
('OrdinaryMaintenance','Complete Chain Lubrication','CST0000001','service@polytech.com','2010-06-03 15:00:00'),
('HardwareUpgrade','Added Weather station','CST0000001','info@parkservice.com','2010-06-15 19:00:00'),
('OrdinaryMaintenance','Complete Chain Lubrication','CST0000001','manager@polytech.com','2010-07-03 8:30:00'),
('OrdinaryMaintenance','Complete Chain Lubrication','CST0000001','manager@polytech.com','2010-08-03 10:30:00'),
('OrdinaryMaintenance','Complete Chain Lubrication','CST0000001','manager@polytech.com','2010-09-03 9:30:00'),
('OrdinaryMaintenance','Brake test','TLF0000001','service@polytech.com','2010-08-03 10:30:00'),
('OrdinaryMaintenance','brake test','TLF0000001','service@polytech.com','2010-09-03 9:30:00'),
('OrdinaryMaintenance','Complete Chain Lubrication','CST0000002','manager@polytech.com','2010-08-03 10:30:00'),
('OrdinaryMaintenance','Complete Chain Lubrication','CST0000002','manager@polytech.com','2010-09-03 9:30:00');

-- Tags

INSERT INTO Amusementrides.tag (name) VALUES
('Dlc.DCBusV'),
('Dlc.OutCurrA'),
('Dlc.OutVoltV'),
('Dst.OutCurrA'),
('Q0_Pressure'),
('Q1_Temperature'),
('Q2_Humidity'),
('Pressure'),
('Temperature'),
('Humidity');

-- TAG Synonymies 

INSERT INTO Amusementrides.synonym VALUES
('Q0_Pressure','Pressure'),
('Q1_Temperature','Temperature'),
('Q2_Humidity','Humidity');

-- Devices 

INSERT INTO Amusementrides.Device(id,type,rideid) VALUES
('0212344648','PLC','CST0000001'),
('6859034686','WeatherStation','CST0000001'),
('4456121684','PLC','CST0000002'),
('8415131551','WeatherStation','CST0000002'),
('4568941654','PLC','TLF0000001'),
('8975613254','WeatherStation','TLF0000001');

-- Metatags

INSERT INTO Amusementrides.metatag(name, description) VALUES
('Position1', 'Load (Kg) in position 1'),
('Position2', 'Load (Kg) in position 2'),
('Position3', 'Load (Kg) in position 3'),
('Position4', 'Load (Kg) in position 4'),
('Position5', 'Load (Kg) in position 5'),
('Position6', 'Load (Kg) in position 6'),
('Position7', 'Load (Kg) in position 7'),
('Position8', 'Load (Kg) in position 8'),
('Position9', 'Load (Kg) in position 9'),
('Position10', 'Load (Kg) in position 10'),
('Position11', 'Load (Kg) in position 11'),
('Position12', 'Load (Kg) in position 12'),
('WetRail', '0 if the rail is dry, 1 if the rail is wet'),
('Notes', 'additional notes');

-- Sessions 

INSERT INTO Amusementrides.session(starttime, filename, type,addedby,dateadded,rideid) VALUES
('2010-05-09 10:00:00+02', 'HighMountains_0001.csv','Monitoring','John@acroparkny.com','2010-10-01 19:30:00+02','CST0000001'),
('2010-05-10 10:00:00+02', 'HighMountains_0002.csv','Monitoring','John@acroparkny.com','2010-10-01 19:30:00+02','CST0000001'),
('2010-05-11 10:00:00+02', 'HighMountains_0003.csv','Monitoring','John@acroparkny.com','2010-10-01 19:30:00+02','CST0000001'),
('2010-05-12 10:00:00+02', 'HighMountains_0004.csv','Monitoring','Paul@acroparkny.com','2010-10-01 19:30:00+02','CST0000001'),
('2010-06-13 10:00:00+02', 'HighMountains_0005.csv','Monitoring','John@acroparkny.com','2010-10-01 19:30:00+02','CST0000001');

-- Metatag values

INSERT INTO Amusementrides.Metavalue VALUES
(1,'Position1', '80'),
(1,'Position2', '0'),
(1,'Position3', '0'),
(1,'Position4', '80'),
(1,'Position5', '40'),
(1,'Position6', '70'),
(1,'Position7', '0'),
(1,'Position8', '0'),
(1,'Position9', '0'),
(1,'Position10', '0'),
(1,'Position11', '0'),
(1,'Position12', '0'),
(1,'WetRail', '0'),
(1,'Notes', 'Bad Weather, few people'),
(2,'Position1', '80'),
(2,'Position2', '0'),
(2,'Position3', '0'),
(2,'Position4', '80'),
(2,'Position5', '40'),
(2,'Position6', '70'),
(2,'Position7', '0'),
(2,'Position8', '0'),
(2,'Position9', '0'),
(2,'Position10', '0'),
(2,'Position11', '0'),
(2,'Position12', '0'),
(2,'WetRail', '0'),
(3,'WetRail', '0'),
(3,'Notes', 'Broken Weight sensor, no load information'),
(4,'WetRail', '1'),
(4,'Notes', 'Broken Weight sensor, no load information'),
(5,'WetRail', '1'),
(5,'Notes', 'Broken Weight sensor, no load information');

-- Data Packets

INSERT INTO Amusementrides.packet(sessionid,cycle,tagid,deviceid,dataarray,steparray, starttime) VALUES
(1,1,'Dst.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,15,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:00:00+02'),
(1,2,'Dst.OutCurrA','0212344648','{0,0,0,0,0,3,4,5,11,9,8,15,9,8,5,4,4,4,4,3,4,9,10,12,16,7,13,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:00:00+02'),
(1,3,'Dst.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,18,9,8,5,4,4,3,4,8,3,5,7,7,9,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:02:00+02'),
(2,1,'Dst.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,15,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:04:00+02'),
(2,2,'Dst.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,7,15,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:08:00+02'),
(2,3,'Dst.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,19,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:00:00+02'),
(2,4,'Dst.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,15,9,21,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:02:00+02'),
(3,1,'Dst.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,15,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:00:00+02'),
(4,1,'Dst.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,10,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:00:00+02'),
(5,1,'Dst.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,15,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:02:00+02'),
(5,2,'Dst.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,18,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:08:00+02'),	
(1,1,'Dlc.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,15,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:00:00+02'),
(1,2,'Dlc.OutCurrA','0212344648','{0,0,0,0,0,3,4,5,11,9,8,15,9,8,5,4,4,4,4,3,4,9,10,12,16,7,13,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:00:00+02'),
(1,3,'Dlc.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,18,9,8,5,4,4,3,4,8,3,5,7,7,9,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:02:00+02'),
(2,1,'Dlc.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,15,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:04:00+02'),
(2,2,'Dlc.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,7,15,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:08:00+02'),
(2,3,'Dlc.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,19,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:00:00+02'),
(2,4,'Dlc.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,15,9,21,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:02:00+02'),
(3,1,'Dlc.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,15,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:00:00+02'),
(4,1,'Dlc.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,10,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:00:00+02'),
(5,1,'Dlc.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,15,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:02:00+02'),
(5,2,'Dlc.OutCurrA','0212344648','{0,0,0,0,0,1,2,3,4,10,11,18,9,8,5,4,4,4,4,3,4,5,6,7,16,7,3,1,0,0,0,0}', '{0,28,32,31,31,31,31,32,31,31,31,31,32,46,32,31,31,28,35,31,32,31,31,31,31,32,31,31,31,31}','2010-05-09 10:08:00+02');	

UPDATE AmusementRides.session SET numcycles=numcycles+3 WHERE id=1;
UPDATE AmusementRides.session SET numcycles=numcycles+4 WHERE id=2;
UPDATE AmusementRides.session SET numcycles=numcycles+1 WHERE id=3;
UPDATE AmusementRides.session SET numcycles=numcycles+1 WHERE id=4;
UPDATE AmusementRides.session SET numcycles=numcycles+2 WHERE id=5;

-- Features

INSERT INTO Amusementrides.Feature VALUES
('peak1','First Peak of the variable in the packet');

-- Feature VALUES

INSERT INTO Amusementrides.Featurevalue VALUES
('peak1',1,15),
('peak1',2,11),
('peak1',3,18),
('peak1',4,15),
('peak1',5,10),
('peak1',6,19),
('peak1',7,15),
('peak1',8,15),
('peak1',9,11),
('peak1',10,15),
('peak1',11,18),
('peak1',12,15),
('peak1',13,11),
('peak1',14,18),
('peak1',15,15),
('peak1',16,10),
('peak1',17,19);

-- Feature references

INSERT INTO Amusementrides.FeatureReference VALUES
('peak1','CST0000001','Dst.OutCurrA',14);


