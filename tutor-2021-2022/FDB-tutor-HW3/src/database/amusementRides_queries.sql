

-- Given the Park ID, view the details about the maintenance events performed on the rides installed in the park, starting from the most recent.
-- Together with rhe event data we show the information about the technician who performed the event and its company with reference email

SELECT p.name as Park,e.dateperformed::date as Date, r.name as Ride, e.type, e. description, u.first_name||' '||u.last_name as Technician, c.name as Company, c.email as ReferenceEmail
FROM Amusementrides.Park as p
	INNER JOIN Amusementrides.ride as r on r.parkid=p.name
	INNER JOIN Amusementrides.event as e on e.rideid=r.id 
	INNER JOIN Amusementrides.user as u ON e.userid=u.email
	INNER JOIN Amusementrides.Company as c ON u.companyID=c.vat
WHERE p.id='AcroparkNY'
ORDER BY Date DESC;

-- Print Ride list with park information and number of performed sessions

SELECT r.id, r.modelid, p.name as Park, r.name as Name_in_park, sc.ns as number_of_sessions
FROM Amusementrides.ride as r 
	INNER JOIN Amusementrides.park as p ON r.parkid=p.name
	LEFT OUTER JOIN (
					SELECT r2.id, COUNT(s.id) as ns
					FROM Amusementrides.ride as r2 
						LEFT JOIN Amusementrides.session as s ON r2.id=s.rideid
					GROUP BY r2.id
					) as sc ON sc.id=r.id;
					  
	
-- Select sessions for which there are packets without associated features

SELECT s.id AS sessionid, s.filename AS filename, ARRAY_AGG(DISTINCT p.id) AS missing_features_on_packets
FROM Amusementrides.packet AS p
	INNER JOIN Amusementrides.Session AS s on s.id=p.sessionid
	LEFT JOIN Amusementrides.Featurevalue AS fv on fv.packetid=p.id
WHERE fv.featureid IS NULL
GROUP BY s.id;


-- given a ride and a feature, see all the packets with associated features together with the reference value

SELECT s.id assessionid, p.id as packetid, p.tagid, fv.value as feature_value, r.value as reference_value
FROM Amusementrides.packet as p
	INNER JOIN Amusementrides.session as s ON s.id=p.sessionid
	INNER JOIN Amusementrides.featurevalue as fv ON fv.packetid=p.id
	INNER JOIN Amusementrides.featurereference as r ON r.rideid=s.rideid AND r.featureid=fv.featureid AND r.tagid=p.tagid
WHERE fv.featureid='peak1' AND s.rideid='CST0000001'
ORDER BY sessionid,p.tagid;

-- given a feature, for each session and for each tag get the number of packets with feature value greater than the reference, only if this number is 2 or more

SELECT p.sessionid, p.tagid, COUNT(p.id) as overref
FROM Amusementrides.packet as p
	INNER JOIN Amusementrides.session as s ON s.id=p.sessionid
	INNER JOIN Amusementrides.featurevalue as fv ON fv.packetid=p.id
	INNER JOIN Amusementrides.featurereference as r ON r.rideid=s.rideid AND r.featureid=fv.featureid AND r.tagid=p.tagid
WHERE fv.value>r.value AND fv.featureid='peak1'
GROUP BY p.sessionid, p.tagid
HAVING COUNT(p.id)>1;
