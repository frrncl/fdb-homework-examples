/*
 * Copyright 2020 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Prints the park data together with its rides and events perfomred on the rides
 *
 * @author Luca Piazzon
 * @version 1.00
 */
public class Amusementrides {

    /**
     * The JDBC driver to be used
     */
    private static final String DRIVER = "org.postgresql.Driver";

    /**
     * The URL of the database to be accessed
     */
    private static final String DATABASE = "jdbc:postgresql://localhost/tutordb";

    /**
     * The username for accessing the database
     */
    private static final String USER = "parkadmin";

    /**
     * The password for accessing the database
     */
    private static final String PASSWORD = "1234";

    /**
     * List all the rides with the performed maintenance events, gropued by park
     *
     * @param args command-line arguments (not used).
     */
    public static void main(String[] args) {

        // the connection to the DBMS
        Connection con = null;
        // the statement to be executed
        Statement stmt = null;
        Statement stmt2 = null;
        // the results of the statements execution
        ResultSet rs = null;
        ResultSet rs2= null;
        // start time of a statement
        long start;
        // end time of a statement
        long end;

        // "data structures" for the data to be read from the database

        try {
            // register the JDBC driver
            Class.forName(DRIVER);
            System.out.printf("Driver %s successfully registered.%n", DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.printf(
                    "Driver %s not found: %s.%n", DRIVER, e.getMessage());
            // terminate with a generic error code
            System.exit(-1);
        }
        try {
            // connect to the database
            start = System.currentTimeMillis();
            con = DriverManager.getConnection(DATABASE, USER, PASSWORD);
            end = System.currentTimeMillis();
            System.out.printf(
                    "Connection to database %s successfully established in %,d milliseconds.%n",
                    DATABASE, end-start);
            // create the statement to execute the query
            start = System.currentTimeMillis();
            stmt = con.createStatement();
            end = System.currentTimeMillis();
            System.out.printf(
                    "Statement successfully created in %,d milliseconds.%n",
                    end-start);

            //get all the parks
            String sql="SELECT * from Amusementrides.park;";
            // execute the query
            start = System.currentTimeMillis();
            rs = stmt.executeQuery(sql);

            System.out.printf("%nEvent List:%n");

            String name;
            String email;

            // cycle on the query results (i.e. for each park we will select the events to be printed)
            while (rs.next()) {

                // read the diagnosis
                name = rs.getString("name");

                // read the date scheduled for the visit
                email = rs.getString("email");

                System.out.printf("%nPark %s - Reference Email: %s%n",
                        name, email);
                // set up the query to get the list of events performed on the rides installed in the current park
                sql="SELECT p.name as Park,e.dateperformed::date as Date, r.name as Ride, e.type, e. description, u.first_name, u.last_name, c.name as Company, c.email as ReferenceEmail" +
                        " FROM Amusementrides.Park as p INNER JOIN Amusementrides.ride as r on r.parkid=p.name INNER JOIN Amusementrides.event as e on e.rideid=r.id " +
                        "INNER JOIN Amusementrides.user as u ON e.userid=u.email INNER JOIN Amusementrides.Company as c ON u.companyID=c.vat WHERE p.name='"+name+"' ORDER BY Ride,Date DESC";
                stmt2 = con.createStatement();
                rs2 = stmt2.executeQuery(sql);
                String Ride;
                String Technician;
                String Date;
                String Company;
                String companyEmail;
                String Description;
                String prev="";

                if (!rs2.isBeforeFirst()){ // if there are no events related to this park
                    System.out.printf("There are no events in %s park%n",name);
                }
                else {
                    while (rs2.next()) { // if there is at least one event
                        Ride = rs2.getString("ride");
                        Technician = rs2.getString("first_name") + ' ' + rs2.getString("last_name");
                        Date = rs2.getString("date");
                        Company = rs2.getString("company");
                        companyEmail = rs2.getString("referenceemail");
                        Description = rs2.getString("description");
                        if (!Ride.equals(prev)) { // if the current ride is equal to the previous row, we continue to write events
                                                    // if the current ride is different from the previous row, we start the new list related to the new ride
                            System.out.printf("%n  - Ride: %s%n", Ride);
                            System.out.printf("    DATE\t\t\tCOMPANY\t\t\tEMAIL\t\t\t\t\t\tTECHNICIAN\t\t\tDESCRIPTION%n");
                            prev = Ride;
                        }
                        System.out.printf("    %s\t\t%s\t\t%s\t\t%s\t\t\t%s%n", Date, Company, companyEmail, Technician, Description);
                    }
                }
                rs2.close();
            }
            rs.close();
            stmt.close();
            con.close();
            end=System.currentTimeMillis();
            System.out.printf("%nData correctly exctracted and visualized in %d milliseconds.%n",end - start);
        } catch (SQLException e) {
            System.out.printf("Database access error:%n");

            // cycle in the exception chain
            while (e != null) {
                //e.printStackTrace();
                System.out.printf("- Message: %s%n", e.getMessage());
                System.out.printf("- SQL status code: %s%n", e.getSQLState());
                System.out.printf("- SQL error code: %s%n", e.getErrorCode());
                System.out.printf("%n");
                e = e.getNextException();
            }
        } finally {
            try {
                // close the used resources
                if (!rs.isClosed() || !rs2.isClosed()) {

                    start = System.currentTimeMillis();

                    rs.close();
                    rs2.close();
                    end = System.currentTimeMillis();

                    System.out
                            .printf("Result set successfully closed in finally block in %,d milliseconds.%n",
                                    end-start);
                }

                if (!stmt.isClosed()) {

                    start = System.currentTimeMillis();

                    stmt.close();

                    end = System.currentTimeMillis();

                    System.out
                            .printf("Statement successfully closed in finally block in %,d milliseconds.%n",
                                    end-start);
                }

                if (!con.isClosed()) {

                    start = System.currentTimeMillis();

                    con.close();

                    end = System.currentTimeMillis();

                    System.out
                            .printf("Connection successfully closed in finally block in %,d milliseconds.%n",
                                    end-start);
                }

            } catch (SQLException e) {
                System.out.printf("Error while releasing resources in finally block:%n");

                // cycle in the exception chain
                while (e != null) {
                    System.out.printf("- Message: %s%n", e.getMessage());
                    System.out.printf("- SQL status code: %s%n", e.getSQLState());
                    System.out.printf("- SQL error code: %s%n", e.getErrorCode());
                    System.out.printf("%n");
                    e = e.getNextException();
                }

            } finally {

                // release resources to the garbage collector
                rs = null;
                stmt = null;
                con = null;

                System.out.printf("Resources released to the garbage collector.%n");
            }
        }

        System.out.printf("Program end.%n");

    }
}


