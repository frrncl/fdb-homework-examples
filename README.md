# Foundations of Databases (FDB) - Homework Examples

This repository contains examples of homework carried out by the tutors of the *Foundations of Databases* course.

*Foundations of Databases* is a course of the 

* [Master Degree in ICT for Internet and Multimedia](https://degrees.dei.unipd.it/master-degrees/ict-for-internet-multimedia-mime/)
* [Master Degree in Computer Engineering](https://degrees.dei.unipd.it/master-degrees/computer-engineering/)
* [Master Degree in Cybersecurity](https://cybersecurity.math.unipd.it/)

of the  [Department of Information Engineering](https://www.dei.unipd.it/en/), [University of Padua](https://www.unipd.it/en/), Italy. 

*Foundations of Databases* is part of the teaching activities of the [Intelligent Interactive Information Access (IIIA) Hub](http://iiia.dei.unipd.it/).
